
import './styles/main.scss';

//SELECTORS
const headerName = document.querySelector('.header__info-name');
const headerPhone = document.querySelector('.header__info-phone');
const headerLocation = document.querySelector('.header__info-location');

const mainName = document.querySelector('.main-body__text--name');
const mainWebsite = document.querySelector('.main-body__text--website');
const mainPhone = document.querySelector('.main-body__text--phone');
const mainLocation = document.querySelector('.main-body__text--location');




//pages
const pages = document.querySelectorAll('.main');




// NAV
//functionality: click on nav buttons => changes render different page

const navBtns = document.querySelectorAll('.nav__btn');

navBtns.forEach(elem => {
    elem.addEventListener('click', fireButtonClick);

});

function fireButtonClick(e){
    //remove nav__btn--active class
    navBtns.forEach(elem => {
        elem.classList.remove('nav__btn--active');
    });
    
    //add nav__btn--active onto clicked item
    e.target.classList.add('nav__btn--active');

    //render appropriate page
    //first: add hide classList to all pages
    pages.forEach(elem => {
        if (!elem.classList.contains('hide')){
            elem.classList.add('hide');
        }
        
    })

    const specificPage = e.target.textContent.toLowerCase();
    // remove hide class from specificPage
    document.querySelector(`.${specificPage}`).classList.remove('hide');


    //hide edit page if its open
    document.querySelector('.edit').classList.add('hide');
    

}





// POPUP
//functionaloty: on icon click => display popup 

const popupBtns = document.querySelectorAll('.inner-icon');

popupBtns.forEach(elem => {
    elem.addEventListener('click', (e) => {
        //remove hide classList from previousSibling
        e.target.previousElementSibling.classList.remove('hide');

        //display content of input
        let text = e.target.previousElementSibling.previousElementSibling.textContent;
        e.target.previousElementSibling.children[0].children[0].value = text

    })
});



//CANCEL BTN inside POPUP
//functionality:  hide POPUP when CANCEL button is clicked

const cancelBtns = document.querySelectorAll('.form__btn--cancel');
cancelBtns.forEach(elem => {
    elem.addEventListener('click', (e) => {
        e.preventDefault();
        e.target.parentElement.parentElement.classList.remove('error');
        e.target.parentElement.parentElement.parentElement.classList.add('hide');
    })
});


//SAVE BTN inside POPUP
// functionality: on click => IF inputs are empty display error,  ELSE  update fields

const saveBtns = document.querySelectorAll('.form__btn--save');
saveBtns.forEach(elem => {
    elem.addEventListener('click', (e) => {
        e.preventDefault();
        // let inputText = 
        // console.log(e.target.parentElement.parentElement.parentElement.previousElementSibling);
        let inputText = e.target.parentElement.previousElementSibling.previousElementSibling.value.trim();
        // console.log(inputText);
        if (inputText){
            
            //check which field to update
            let fieldElem = e.target.parentElement.parentElement.parentElement.previousElementSibling;

            if (fieldElem.classList.contains('main-body__text--name')){
                headerName.textContent = inputText;
                mainName.textContent = inputText;
            }else if (fieldElem.classList.contains('main-body__text--website')){
                mainWebsite.innerHTML = `<ion-icon name="globe" class="icon"></ion-icon> ${inputText}`;

            }else if (fieldElem.classList.contains('main-body__text--phone')){
                headerPhone.innerHTML = `<ion-icon name="call" class="icon"></ion-icon> ${inputText}`;
                mainPhone.innerHTML = `<ion-icon name="call" class="icon"></ion-icon> ${inputText}`;

            }else if (fieldElem.classList.contains('main-body__text--location')){
                headerLocation.innerHTML = `<ion-icon name="pin" class="icon"></ion-icon> ${inputText}`;
                mainLocation.innerHTML = `<ion-icon name="home" class="icon"></ion-icon> ${inputText}`;

            }

            //clear inputText
            e.target.parentElement.previousElementSibling.value = '';

            //hide popup
            // console.log(e.target.parentElement.parentElement.parentElement);
            e.target.parentElement.parentElement.parentElement.classList.add('hide');


            //remove error class if present
            e.target.parentElement.parentElement.classList.remove('error');


        }else{
            // add error class
            e.target.parentElement.parentElement.classList.add('error');
        }


    })
})




/////EDITING ON MOBILE
// edit functionality for mobile

const editIcon = document.querySelector('.main-header__icon');
const editCancel = document.querySelector('.edit-header__btn--cancel');
const editSave = document.querySelector('.edit-header__btn--save');

//input fields
const firstNameInput = document.querySelector('.form__input--first');
const lastNameInput = document.querySelector('.form__input--last');
const websiteInput = document.querySelector('.form__input--website');
const phoneInput = document.querySelector('.form__input--phone');
const cityInput = document.querySelector('.form__input--city');


editIcon.addEventListener('click', (e) => {

    //hide about section
    e.target.parentElement.parentElement.classList.add('hide');
    //display edit section
    const editSection = document.querySelector('.edit');
    editSection.classList.remove('hide');

    //get existing data
    let data = getData();
    
    //fill in inputs with appropriate data
    firstNameInput.value = data.firstName;
    lastNameInput.value = data.lastName;
    websiteInput.value = data.website;
    phoneInput.value = data.phone;
    cityInput.value = data.city;
});

//getData function
///retrieve data from input fields 
function getData(){
    let nameArr = mainName.textContent.trim().split(' ');

    let obj = {
        firstName: nameArr[0],
        lastName: nameArr[1],
        website: mainWebsite.textContent,
        phone: mainPhone.textContent,
        city: mainLocation.textContent

    };

    return obj;
}


//CANCEL btn clicked

editCancel.addEventListener('click', (e) => {

    //hide edit window
    e.target.parentElement.parentElement.parentElement.classList.add('hide');
    //display about window
    document.querySelector('.about').classList.remove('hide');
    

    
});


//SAVE btn clicked

editSave.addEventListener('click', (e) => {

    //check if all fields are filled
    let data = {
        firstNameInput: firstNameInput.value.trim(),
        lastNameInput: lastNameInput.value.trim(),
        websiteInput: websiteInput.value,
        phoneInput: phoneInput.value,
        cityInput: cityInput.value
    }

    let decision = true;
    for (let key in data){
        if (data[key].trim() === ''){
            decision = false;
        }
    };

    if (decision){
        //update fields
        mainName.textContent = `${firstNameInput.value} ${lastNameInput.value}`;
        headerName.textContent = `${firstNameInput.value} ${lastNameInput.value}`;
        mainWebsite.innerHTML = `<ion-icon name="globe" class="icon"></ion-icon>${websiteInput.value}`;
        mainPhone.innerHTML = `<ion-icon name="call" class="icon"></ion-icon>${phoneInput.value}`;
        headerPhone.innerHTML = `<ion-icon name="call" class="icon"></ion-icon>${phoneInput.value}`;
        mainLocation.innerHTML = `<ion-icon name="home" class="icon"></ion-icon>${cityInput.value}`;
        headerLocation.innerHTML= `<ion-icon name="pin" class="icon"></ion-icon>${cityInput.value}`;

        //clear fields
        firstNameInput.value = '';
        lastNameInput.value = '';
        websiteInput.value = '';
        phoneInput.value = '';
        cityInput.value = '';

        //hide edit window
        e.target.parentElement.parentElement.parentElement.classList.add('hide');
        //display about window
        document.querySelector('.about').classList.remove('hide');
    }else{
        // console.log('error');
        //create error element
        const div = document.createElement('div');
        div.classList.add('divError');

        const p = document.createElement('p');
        p.classList.add('textError');
        p.textContent = 'Please fill in all fields';

        div.appendChild(p);
        // console.log(div);
        // insert error element into dom
        document.querySelector('.edit__header').insertAdjacentElement('afterend', div);


        setTimeout(() => {
            document.querySelector('.divError').remove();
        }, 2000);
    }

    

})



////////////////
// MATERIAL DESIGN FOR INPUT
//PART 1 => POPUP

const inputs = document.querySelectorAll('.popup__input');

inputs.forEach(elem => {

    elem.addEventListener('focus', (e) => {
        const input = e.target.value;
        if (input.trim() === ''){
            e.target.nextElementSibling.classList.add('red');

        }
    })
})

inputs.forEach(elem => {

    elem.addEventListener('focusout', (e) => {
        const input = e.target.value;
        if (input.trim() !== ''){
            e.target.nextElementSibling.classList.remove('red');

        }else{
            e.target.nextElementSibling.classList.add('red');
        }
    });
});

inputs.forEach(elem => {

    elem.addEventListener('click', (e) => {
        e.target.nextElementSibling.classList.remove('red');
    })

})

inputs.forEach(elem => {

    elem.addEventListener('keypress', (e) => {
        e.target.parentElement.classList.remove('error');
    })
})

//cancel btn => remove red class
const cancelBtn = document.querySelectorAll('.form__btn--cancel');

cancelBtn.forEach(elem => {

    elem.addEventListener('click', (e) => {
        e.target.parentElement.previousElementSibling.classList.remove('red');
    });
});



//PART 2 => POPUP

const editInputs = document.querySelectorAll('.edit-form__input');

editInputs.forEach(elem => {
    
    elem.addEventListener('focus', (e) => {

        const input = e.target.value;
        if (input.trim() === ''){
            e.target.nextElementSibling.classList.add('editRed');
        }
    })

});

editInputs.forEach(elem => {

    elem.addEventListener('focusout', (e) => {
        const input = e.target.value;
        if (input.trim() !== ''){
            e.target.nextElementSibling.classList.remove('editRed');

        }else{
            e.target.nextElementSibling.classList.add('editRed');
        }
    })
});

editInputs.forEach(elem => {

    elem.addEventListener('click', (e) => {
        e.target.nextElementSibling.classList.remove('editRed');
    })
})


//cancel btn pressed => remove editRed from all inputs
const editCancelBtn = document.querySelector('.edit-header__btn--cancel');
editCancelBtn.addEventListener('click', () => {
    document.querySelectorAll('.edit-form__label').forEach(elem => {
        elem.classList.remove('editRed');
    })
})
